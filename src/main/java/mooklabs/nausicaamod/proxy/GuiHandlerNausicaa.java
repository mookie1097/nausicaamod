package mooklabs.nausicaamod.proxy;

import mooklabs.nausicaamod.Main;
import mooklabs.nausicaamod.godwarrior.ContainerGodWarrior;
import mooklabs.nausicaamod.godwarrior.GodWarriorControlGui;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import bcblocks.bettertable.BetterTable;
import bcblocks.bettertable.table.BetterTableGui;
import bcblocks.bettertable.table.ContainerBetterTable;
import bcblocks.bettertable.tileentity.TileEntityDeviceCraftingTable;
import cpw.mods.fml.common.network.IGuiHandler;

public class GuiHandlerNausicaa implements IGuiHandler {

	@Override
	public Object getServerGuiElement(int id, EntityPlayer player, World world, int x, int y, int z) {
		TileEntity tileEntity = world.getTileEntity(x, y, z);
		switch (id) {
		case Main.godControlGuiID:
			return player != null ? new ContainerGodWarrior(player.inventory):null;
			
		}
		return null;
	}

	@Override
	public Object getClientGuiElement(int id, EntityPlayer player, World world, int x, int y, int z) {
		TileEntity tileEntity = world.getTileEntity(x, y, z);
		switch (id) {
		case Main.godControlGuiID:
			
				return player != null ? new GodWarriorControlGui(new ContainerGodWarrior(player.inventory)):null;
			

		}
		return null;
	}
}